#include <iostream>
#include <climits>

int
main()
{ 
    int counter = 1;
    float largest = INT_MIN;
    
    while (counter <= 10) {
        float number;
        
        std::cout << "Enter a value(" << counter << " of 10): ";
        std::cin >> number;

        if (number > largest) {
            largest = number;
        }
    
        ++counter;
    }

    std::cout << "The largest value: " << largest << std::endl;

    return 0;
}
