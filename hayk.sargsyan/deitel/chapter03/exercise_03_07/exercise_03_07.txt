Class consists of some member functions that manipulate the object's attributes.
Attributes are represented as variables in a class definition. That variables
are called data members. They are declared inwardly in the class.
