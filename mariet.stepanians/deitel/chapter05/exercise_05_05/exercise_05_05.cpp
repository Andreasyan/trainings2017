#include <iostream>

int
main()
{
    int amountOfNumbers;

    std::cout << "Enter the amount of numbers you want to add: ";
    std::cin >> amountOfNumbers;

    int sum = 0;
    
    for (int i = 1; i <= amountOfNumbers; ++i) {
        int number;
        std::cout << "Enter the number: ";
        std::cin  >> number;
        sum += number; 
    }
    std::cout << "Sum -> " << sum << std::endl;

    return 0;
}
