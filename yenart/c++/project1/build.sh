echo "Pre-processing main.cpp to main.ii"
g++ -E main.cpp -o main.ii || { echo "Error 1: Pre-processing failed" && exit 1; }


echo "Compiling main.ii to main.s"
g++ -S main.ii -o main.s || { echo "Error 2: Compilation failed" && exit 2; }


echo "Assembling main.s to main.o"
g++ -c main.s -o main.o || { echo "Error 3: Assemblinging failed" && exit 3; }


echo "Linking main.o to program"
g++ main.o -o program || { echo "Error 4: Linking failed" && exit 4; }

echo program > .gitignore

echo "Done"

