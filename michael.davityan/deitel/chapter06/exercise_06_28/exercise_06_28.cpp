#include <iostream>

double theSmallestOfThree(const double number1, const double number2, const double number3);

int
main()
{
    double number1;
    std::cin >> number1;
    double number2;
    std::cin >> number2;
    double number3;
    std::cin >> number3;
    std::cout << theSmallestOfThree(number1, number2, number3) << std::endl;

    return 0;
}

double 
theSmallestOfThree(const double number1, const double number2, const double number3)
{
    double minimum = number1;
    if (number2 < minimum) {
        minimum = number2;
    }
    if (number3 < minimum) {
        minimum = number3;
    }

    return minimum;
}
