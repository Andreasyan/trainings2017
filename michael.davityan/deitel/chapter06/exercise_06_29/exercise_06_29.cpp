#include <iostream>
#include <cmath>

bool perfect(const int number);
void printDivisors (const int number);

int
main()
{
    int numberLimit;
    std::cin >> numberLimit;
    if (numberLimit < 0) {
        std::cout << "Error 1: wrong number limit." << std::endl;
        return 1;
    }
    for (int number = 1; number <= numberLimit; ++number) {
        perfect(number);
    }
    
    return 0;
}

bool
perfect(const int number)
{
    int perfectOrNo = 0;
    const int userNumber = std::abs(number);
    for (int divisor = 1; divisor <= userNumber / 2; ++divisor) {
        if (0 == userNumber % divisor) {
            perfectOrNo += divisor;
        }
    } 
    if (userNumber == perfectOrNo) {
        printDivisors(number);
        return true;
    }
    return false;
}

void 
printDivisors(const int number) {
    const int userNumber = std::abs(number);
    std::cout << "Perfect Number: " << number << "\t" << "Divisors:";
    for (int divisor = 1; divisor <= userNumber / 2; ++divisor) {
        if (0 == userNumber % divisor) {
            if (number < 0) {
                std::cout << " " << -1 * divisor;
                continue;
            }
        std::cout << " " << divisor;
        }
    }
    std::cout << std::endl;
}
