#include <iostream>

bool even(const int number);

int
main() 
{
    int numberQuantity;
    std::cin >> numberQuantity;
    std::cout << "Number\t" << "Value\t" << "Even(true 1.,false 0.)\n";
    for (int counter = 1; counter <= numberQuantity; ++counter) {
        int number;
        std::cin >> number;
        std::cout << "  " << counter << "\t  " << number << "\t  " << even(number) << std::endl;
    }
    return 0;
}

bool
even(const int number)
{
    return (0 == number % 2 ? true : false);
} 

