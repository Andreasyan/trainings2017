#include "Date.hpp"

#include <iostream>

int
main()
{
    Date date1(11, 18, 2016);
    date1.displayDate();

    Date date2(1, 0, 0);

    int month;
    std::cout << "Enter month: ";
    std::cin >> month;
    date2.setMonth(month);

    int day;
    std::cout << "Enter day: ";
    std::cin >> day;
    date2.setDay(day);

    int year;
    std::cout << "Enter year: ";
    std::cin >> year;
    date2.setYear(year);

    date2.displayDate();

    return 0;
}

