#include <iostream>
#include <cmath>

int
main()
{
    int size = 8;
    int halfSize = size / 2;
    for (int row = -halfSize; row <= halfSize; ++row) {
        for (int column = -halfSize; column <= halfSize; ++column) {
            std::cout << (std::abs(column) + std::abs(row) <= halfSize ?  "*" : " " );
        }
        std::cout << std::endl;
    }
    return 0;
}

