#include <iostream>
#include <iomanip>

int
main()
{
    for (int row = 1; row <= 10; ++row) {
        for (int column = 1; column <= row; ++column) {
            std::cout << '*';
        }

        std::cout << std::setw(13 - row);
        for (int column = 10; column >= row; --column) {
            std::cout << '*';
        }
 
        std::cout << std::setw(1 + 2 * row);
        for (int column = row; column <= 10; ++column) {
            std::cout << '*';
        }

        std::cout << std::setw(13 - row);
        for (int column = 10 - row; column < 10; ++column) {
            std::cout << '*';
        }
        std::cout << std::endl;
    }

    return 0;
}

