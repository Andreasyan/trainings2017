#include <iostream>

int
main()
{
    double side1;
    std::cout << "Enter value of side1: ";
    std::cin >> side1;
    if (side1 <= 0) {
        std::cout << "Error 1: Side's value can not be negative." << std::endl;
        return 1;
    }

    double side2;
    std::cout << "Enter value of side2: ";
    std::cin >> side2;
    if (side2 <= 0) {
        std::cout << "Error 1: Side's value can not be negative." << std::endl;
        return 1;
    }

    double side3;
    std::cout << "Enter value of side3: ";
    std::cin >> side3;
    if (side3 <= 0) {
        std::cout << "Error 1: Side's value can not be negative." << std::endl;
        return 1;
    }
    

    double a = side1 * side1;
    double b = side2 * side2;
    double c = side3 * side3;
    if (a == b + c) {
        std::cout << "The triangle is a right." << std::endl;
        return 0;
    } else if (b == a + c) {
        std::cout << "The triangle is a right." << std::endl;
        return 0;
    } else if (c == a + b) {
        std::cout << "The triangle is a right." << std::endl;
        return 0;
    }
    
    if (side1 + side2 > side3) {
        if (side1 + side3 > side2) {
            if (side2 + side3 > side1) {
                std::cout << "It isn't a right triangle. Try again!" << std::endl;
                return 0;
            }
        }
    }
    std::cout << "Triangle cannot be with these parameters." << std::endl;
    return 0;
}

